package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethodsPOM;

public class HomePage extends ProjectMethodsPOM{
	
	public MyHomePage clickCRMSFA()	{
		WebElement eleCRMSFA = locateElement("linkText","CRM/SFA");
		click(eleCRMSFA);
		return new MyHomePage();
		
	}

	

}

