package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethodsPOM;

public class ViewPage extends ProjectMethodsPOM {

	public OpenTapsCRMPage clickLogout()
	{
		WebElement eleLogout = locateElement("linkText","Logout");
		click(eleLogout);
		return new OpenTapsCRMPage();
	}
}
