package pages;

import org.openqa.selenium.WebElement;


import wdMethods.ProjectMethodsPOM;

public class FindLeadPage extends ProjectMethodsPOM{
	
	public FindLeadPage enterFirstName(String firstname)
	{
		WebElement eleFirstName = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(eleFirstName,firstname);
		return this;
	}
	
	
	public ViewPage clickFindLead()	{
		WebElement eleFindLead = locateElement("xpath","//button[contains(text(),'Find Leads')]");
		click(eleFindLead);
		return new ViewPage();
		
	}
	}
	
	



