package pages;

import org.openqa.selenium.WebElement;


import wdMethods.ProjectMethodsPOM;

public class MyHomePage extends ProjectMethodsPOM{
	
	public MyLeadsPage clickLeadstab()	{
		WebElement eleLeads = locateElement("linkText","Leads");
		click(eleLeads);
		return new MyLeadsPage();
		
	}
	
	

}

