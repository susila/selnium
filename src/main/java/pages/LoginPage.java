package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethodsPOM;


public class LoginPage extends ProjectMethodsPOM{
	
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	public LoginPage enterUserName(String username)
	{
		WebElement eleUserName = locateElement("id","username");
		type(eleUserName,username);
		return this;
	}
	public LoginPage enterPassword(String password)
	{
		WebElement elePassword = locateElement("id","password");
		type(elePassword,password);
		return this;
	}
	public HomePage clickLogin()
	{
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		return new HomePage();
	}
	

}
