package pages;

import org.openqa.selenium.WebElement;


import wdMethods.ProjectMethodsPOM;

public class CreateLeadPage extends ProjectMethodsPOM{
	
	public CreateLeadPage enterCompanyName(String companyname)
	{
		WebElement eleCompanyName = locateElement("id","createLeadForm_companyName");
		type(eleCompanyName,companyname);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String firstname)
	{
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		type(eleFirstName,firstname);
		return this;
	}
	public CreateLeadPage enterLastName(String lastname)
	{
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		type(eleLastName,lastname);
		return this;
	}
	
	public ViewPage clickCreateLead()	{
		WebElement eleCreadLead = locateElement("class","smallSubmit");
		click(eleCreadLead);
		return new ViewPage();
		
	}
	}
	
	



