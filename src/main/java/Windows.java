import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class Windows {
	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver.exe/");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("AGENT LOGIN").click();
		Set<String> allwindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allwindows);		
		driver.findElementByLinkText("CONTACT US").click();		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File obj = new File("./Snaps/img1.jpeg");
		FileUtils.copyFile(src, obj);
		

	}

	
}
