package wdMethods;

import java.io.IOException;

//import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

//import testcases.ReadFile;
import testcases.ReadFile1;

public class ProjectMethodsPOM extends SeMethodsPOM{

	@BeforeSuite
	public void beforeSuite() {
		startResult();
	}
	@BeforeMethod
	public void login() throws InterruptedException {
	    beforeMethod();
		startApp("chrome", "http://leaftaps.com/opentaps");
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		Thread.sleep(2000);
		WebElement crm = locateElement("linkText", "CRM/SFA");
		click(crm);*/		
	}
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	
	
	public String data1;
	@DataProvider(name="fetchData")
	public Object[][] fetchdata() throws IOException{
		//return readExcel.getsheet();
		int x=0;
		//ReadFile rf = new ReadFile();
		//Object[][] data = ReadFile.dataRead(x);
	
		return ReadFile1.dataRead(data1,x);
	
		
	}
	
	
	
	/*@DataProvider(name="qa")
	//To Read the xlsheet values using Object
	public Object[][] fetchdata() throws IOException
	{
		int x=0;
		//Object[][] data=readFile2.dataRead(x);
     	Object[][] data=ReadFile.dataRead(x);
		return data;
	}*/
	
	/*@DataProvider(name = "qa")
	public Object[][] fetchData() {
		Object[][] data = new Object[2][5];
		data[0][0] = "TestLeaf";
		data[0][1] = "sarath";
		data[0][2] = "M";
		data[0][3] = "sarath@Testleaf.com";
		data[0][4] = 124567890;
		
		data[1][0] = "IBM";
		data[1][1] = "karthi";
		data[1][2] = "G";
		data[1][3] = "karthi@IBM.com";
		data[1][4] = 1245678912;
		return data;
	}*/
	
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	
}






