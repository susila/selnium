import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Day7 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./Driver/chromedriver.exe/");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
	    Alert alertRef = driver.switchTo().alert();
	    Thread.sleep(1000);
	    alertRef.sendKeys("hi");
	    alertRef.accept();
	    String text = driver.findElementById("demo").getText();
	    if(text.contains("hi")){
	    System.out.println("text = " +text);
	    }
	    }
	  }


