package testCase;

	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
	import wdMethods.ProjectMethodsPOM;

	public class TC002ClickCRMSFA extends ProjectMethodsPOM {
		@BeforeTest
		public void setData() {
			testCaseName = "TC001CreateLead";
			testDesc = "Create a new Lead";
			author = "susila";
			category = "smoke";
			data1 = "AllFive";
			
		}
		
		@Test(dataProvider="fetchData")
		public void  clickcrmsfa(String username, String password){
			
			new LoginPage()
			.enterUserName(username)
			.enterPassword(password)
			.clickLogin();
		
			
			new HomePage()
			.clickCRMSFA();
			
			
			
		}
		
		
	}
